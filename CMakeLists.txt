cmake_minimum_required(VERSION 2.8.4)
project(maze)

add_definitions(-Wall -c -O2 -std=c++11)
aux_source_directory(src SOURCES)

add_executable(maze ${SOURCES})
target_link_libraries(maze SDL2)
